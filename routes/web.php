<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
/*##################dashboard########################*/
Route::get('/dashboard', 'UserController@index');
Route::get('/users', 'UserController@users');
/*##########################Admin dashboard############*/

/*########################Campaigns###############*/
Route::get('/Campaigns', 'CampaignsController@index');
Route::get('/add_Campaign', 'CampaignsController@add_Campaigns');
Route::post('/add_Campaign_data', 'CampaignsController@add_Campaign_data');
Route::get('/delete_campaign/{id}', 'CampaignsController@delete_campaign');
Route::get('/search_campaign/', 'CampaignsController@search_campaign');
Route::get('/fundraiser/', 'CampaignsController@fundraiser');
Route::get('/delete_fundraiser/{id}', 'CampaignsController@delete_fundraiser');
Route::get('/add_fundraiser/', 'CampaignsController@add_fundraiser');
Route::post('/add_fundraiser_data/', 'CampaignsController@add_fundraiser_data');
Route::get('/edit_fundraiser/{id}', 'CampaignsController@edit_fundraiser');
Route::post('/update_fundraiser_data/{id}', 'CampaignsController@update_fundraiser_data');
/*#################################Campaigns#################*/