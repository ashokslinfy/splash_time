<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class CampaignsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result['campaigns']=DB::table('campaigns')->get();
        //print_r($result); die;
        return view('campaigns.campaigns',$result);
    }

     public function add_Campaigns(Request $Request)
    {
      return view('campaigns.add_Campaign');
       //$name = $request->input('name'); 
    }
     public function add_Campaign_data(Request $request)
    { 
       
       $data['name'] = $request->input('name'); 
       $data['campaign_code'] = $request->input('campaign_code'); 
       $data['start_date'] = $request->input('start_date'); 
       $data['end_date'] = $request->input('end_date');
        $data['person_incharge'] = $request->input('person_incharge'); 
        $data['phone'] = $request->input('phone'); 
        $data['email'] = $request->input('email');
        print_r($data); die;
         $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:80480',
        ]);
        
        $photos = count($request->file('images')); 
        print_r($photos); die;
     
        $image = $request->file('image');
        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/images');
        $image->move($destinationPath, $input['imagename']); 
        $data['image']= $_FILES['image']['name']; 

        $result=DB::table("campaigns")->insert($data);
        if($result){
          return  redirect(url('Campaigns'));
        } 
       
    }
    public function delete_campaign($id)
    {
        $result=DB::table('campaigns')->where('id', $id)->delete();
        if($result){
          return  redirect(url('Campaigns'));
        } 
    }

    public function search_campaign(Request $request)
    {
      $val= $request->input('val');
      $data=DB::table('campaigns')->where('name', 'LIKE', '%'.$val.'%')->orwhere('campaign_code', 'LIKE', '%'.$val.'%')->orwhere('email', 'LIKE', '%'.$val.'%')->get();
      //print_r($data); die;
      echo json_encode($data);
     /* foreach ($data as $key => $value) {?>
          <?php echo "<tr><td>$value->id</td><td>$value->name</td><td>$value->campaign_code</td>
          <td>$value->start_date</td><td>$value->end_date</td><td>$value->person_incharge</td><td>$value->phone</td><td>$value->email</td></tr>"; ?> 
      <?php }*/
    }


    public function fundraiser()
    {
         $result['fundraiser']=DB::table('fundraiser')->get();
         // print_r($result); die;
        return view('campaigns.fundraiser',$result);
    }

   public function delete_fundraiser($id)
    {   

        $result=DB::table('fundraiser')->where('id', $id)->delete();
        if($result){
          return  redirect(url('fundraiser'));
        } 
    }


    public function add_fundraiser()
    {
        return view('campaigns.add_fundraiser');
    }

    public function add_fundraiser_data(Request $request)
    {
         $data['name'] = $request->input('name'); 
          $result=DB::table("fundraiser")->insert($data);
        if($result){
          return  redirect(url('fundraiser'));
        }   
    }

    public function edit_fundraiser($id)
    {
       $result['fundraiser']=DB::table("fundraiser")->where('id',$id)->first();
       //print_r($result); die;
       if($result){
         return view('campaigns.edit_fundraiser',$result);
        }       
    }
    public function update_fundraiser_data(Request $request,$id)
    {
        //print_r($id); die;
        $data['name'] = $request->input('name'); 
       $result['fundraiser']=DB::table("fundraiser")->where('id',$id)->update($data);
       //print_r($result); die;
       if($result){
         return  redirect(url('fundraiser'));
        }   
    }
}
