@extends('admin_layout.index')
@section('content')
<section id="main-content">
          <section class="wrapper">

              <div class="row">
                  <div class="col-lg-12 main-chart">
                    <div class="content_title">
                      <h1>Add New Campaign</h1>

                    </div>

                    <div class="col-xs-12 min-height-400">   
                      <div class="row">    
                        <div class="col-xs-12 col-sm-6">   
                          <form class="mt-40" action="{{url('add_Campaign_data')}}" method="POST" enctype='multipart/form-data'>
                          <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                            <div class="col-xs-12">
                              <div class="form-group">
                                <label>Campaign Name</label>
                                <input type="text" name="name" required class="form-control">
                              </div>
                            </div>
                            <div  class="col-xs-12">
                              <div class="form-group">
                                <label for="email">campaign code:</label>
                                <input type="text" name="campaign_code" required class="form-control">
                              </div>
                            </div>
                            <div  class="col-xs-12">
                              <div class="form-group">
                                <label>Start date</label>
                                <input type="date" name="start_date" class="form-control">
                              </div>
                            </div>
                            <div  class="col-xs-12">
                              <div class="form-group">
                                <label>End date</label>
                                <input type="date" name="end_date" class="form-control">
                              </div>
                            </div>
                            <div  class="col-xs-12">
                              <div class="form-group">
                                <label>Person Incharge</label>
                                <input type="text" required name="person_incharge" class="form-control">
                              </div>
                            </div>
                             <div  class="col-xs-12">
                              <div class="form-group">
                                <label>Phone</label>
                                <input type="text" required name="phone" class="form-control">
                              </div>
                            </div>
                            <div  class="col-xs-12">
                              <div class="form-group">
                                <label>Email</label>
                                <input type="email" required name="email" class="form-control">
                              </div>
                            </div>
                            <div  class="col-xs-12">
                              <div class="form-group">
                                <label>image</label>
                                <input type="file" required name="images[]" class="form-control" multiple="multiple">
                              </div>
                            </div>
                            <div  class="col-xs-12">
                              <div class="form-group">
                                <label>&nbsp; &nbsp;</label>
                                <div class="col-xs-12">
                                  <div class="row">
                                    <button type="submit" class="btn btn-success">Add</button>
                                    <button type="button" class="btn btn-warning">Cancel</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div><!-- /col-lg-12 END SECTION MIDDLE -->
                  
                  
      
              </div><!--/row -->
          </section>
      </section>
      @endsection