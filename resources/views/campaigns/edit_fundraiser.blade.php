@extends('admin_layout.index')
@section('content')
<section id="main-content">
          <section class="wrapper">

              <div class="row">
                  <div class="col-lg-12 main-chart">
                    <div class="content_title">
                      <h1>Add Fundraiser</h1>

                    </div>

                    <div class="col-xs-12 min-height-400">   
                      <div class="row">    
                        <div class="col-xs-12 col-sm-6">   
                          <form class="mt-40" action="{{url('update_fundraiser_data')}}/{{$fundraiser->id}}" method="POST" enctype='multipart/form-data'>
                          <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                            <div class="col-xs-12">
                              <div class="form-group">
                                <label>Fundraiser Name</label>
                                <input type="text" name="name" value="{{$fundraiser->name}}" required class="form-control">
                              </div>
                            </div>
                            <div  class="col-xs-12">
                              <div class="form-group">
                                <label>&nbsp; &nbsp;</label>
                                <div class="col-xs-12">
                                  <div class="row">
                                    <button type="submit" class="btn btn-success">Add</button>
                                    <button type="button" class="btn btn-warning">Cancel</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div><!-- /col-lg-12 END SECTION MIDDLE -->
                  
                  
      
              </div><!--/row -->
          </section>
      </section>
      @endsection