@extends('admin_layout.index')
@section('content')

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">

<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12 main-chart">
                <div class="content_title">
                    <h1>Fundraiser</h1>
                </div>
                <div class="content_title col-lg-4 form-inline pull-right">
                    <a href="{{url('add_fundraiser')}}" class="btn btn-success pull-right">Add New</a>
                    <!-- <input type="text" name="search_campaign" id="search_campaign" class="form-control pull-right"> -->
                </div>
                <div class="min-height-400">          
                    <table id="campaign_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Fundraiser Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="campaign_data">
                            @foreach($fundraiser as $key => $value)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$value->name}}</td>
                                
                                <td>
                                    <a href="{{url('edit_fundraiser')}}/{{$value->id}}" class="btn btn-success tooltips" data-original-title="Edit" data-placement="top"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="{{url('delete_fundraiser')}}/{{$value->id}}" class="btn btn-warning tooltips" data-original-title="Delete" data-placement="top"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('#campaign_table').DataTable();
                        });
                    </script>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection