@extends('admin_layout.index')

@section('content')
<section id="main-content">
          <section class="wrapper">

              <div class="row">
                  <div class="col-lg-12 main-chart">
                    <div class="content_title">
                      <h1>Ride Detail <a href="add-user.html" class="btn btn-success pull-right">Add New</a></h1>

                    </div>

                    <div class="table-responsive min-height-400">          
                      <table class="table">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>User Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>1</td>
                            <td>Anna Pitt</td>
                            <td>annapitt@gmail.com</td>
                            <td>9876543210</td>
                            <td>
                            <a href="javascript:void(0)" class="btn btn-success tooltips" data-original-title="Edit" data-placement="top"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            <a href="javascript:void(0)" class="btn btn-warning tooltips" data-original-title="Delete" data-placement="top"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            </td>
                          </tr>
                          <tr>
                            <td>2</td>
                            <td>Anna Pitt</td>
                            <td>annapitt@gmail.com</td>
                            <td>9876543210</td>
                            <td>
                            <a href="javascript:void(0)" class="btn btn-success tooltips" data-original-title="Edit" data-placement="top"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            <a href="javascript:void(0)" class="btn btn-warning tooltips" data-original-title="Delete" data-placement="top"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            </td>
                          </tr>
                          <tr>
                            <td>3</td>
                            <td>Anna Pitt</td>
                            <td>annapitt@gmail.com</td>
                            <td>9876543210</td>
                            <td>
                            <a href="javascript:void(0)" class="btn btn-success tooltips" data-original-title="Edit" data-placement="top"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            <a href="javascript:void(0)" class="btn btn-warning tooltips" data-original-title="Delete" data-placement="top"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            </td>
                          </tr>
                          <tr>
                            <td>4</td>
                            <td>Anna Pitt</td>
                            <td>annapitt@gmail.com</td>
                            <td>9876543210</td>
                            <td>
                            <a href="javascript:void(0)" class="btn btn-success tooltips" data-original-title="Edit" data-placement="top"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            <a href="javascript:void(0)" class="btn btn-warning tooltips" data-original-title="Delete" data-placement="top"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            </td>
                          </tr>
                          <tr>
                            <td>5</td>
                            <td>Anna Pitt</td>
                            <td>annapitt@gmail.com</td>
                            <td>9876543210</td>
                            <td>
                            <a href="javascript:void(0)" class="btn btn-success tooltips" data-original-title="Edit" data-placement="top"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            <a href="javascript:void(0)" class="btn btn-warning tooltips" data-original-title="Delete" data-placement="top"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            </td>
                          </tr>
                          <tr>
                            <td>6</td>
                            <td>Anna Pitt</td>
                            <td>annapitt@gmail.com</td>
                            <td>9876543210</td>
                            <td>
                            <a href="javascript:void(0)" class="btn btn-success tooltips" data-original-title="Edit" data-placement="top"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            <a href="javascript:void(0)" class="btn btn-warning tooltips" data-original-title="Delete" data-placement="top"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            </td>
                          </tr>
                          <tr>
                            <td>7</td>
                            <td>Anna Pitt</td>
                            <td>annapitt@gmail.com</td>
                            <td>9876543210</td>
                            <td>
                            <a href="javascript:void(0)" class="btn btn-success tooltips" data-original-title="Edit" data-placement="top"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            <a href="javascript:void(0)" class="btn btn-warning tooltips" data-original-title="Delete" data-placement="top"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            </td>
                          </tr>
                          <tr>
                            <td>8</td>
                            <td>Anna Pitt</td>
                            <td>annapitt@gmail.com</td>
                            <td>9876543210</td>
                            <td>
                            <a href="javascript:void(0)" class="btn btn-success tooltips" data-original-title="Edit" data-placement="top"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            <a href="javascript:void(0)" class="btn btn-warning tooltips" data-original-title="Delete" data-placement="top"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            </td>
                          </tr>

                        </tbody>
                      </table>
                    </div>
                  </div><!-- /col-lg-12 END SECTION MIDDLE -->
                  
                  
      
              </div><!--/row -->
          </section>
      </section>
      @endsection