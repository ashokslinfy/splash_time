@extends('admin_layout.index')

@section('content')
  <section id="main-content">
          <section class="wrapper">

              <div class="row">
                  <div class="col-lg-12 main-chart">
                  <div class="content_title">
                    <h1>Dashboard</h1>
                  </div>
                    <div class="row stats_info">
                      <div class="col-sm-6">
                        <div class="panel panel-default">
                           <div class="panel-heading"><i class="fa fa-bar-chart"></i> Site Statistics</div>
                            <div class="panel-body">
                            <div class="row">
                              <div class="col-md-6">
                                <a href="#" class="total_rides">
                                  <i class="fa fa-users"></i>
                                  <span class="stats_info_text">Riders</span>
                                  <span class="stats_info_total">100</span>
                                </a>
                              </div>
                              <div class="col-md-6">
                                <a href="#" class="total_drivers">
                                  <i class="fa fa-male"></i>
                                  <span class="stats_info_text">Drivers</span>
                                  <span class="stats_info_total">100</span>
                                </a>
                            </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <a href="#" class="total_compnis">
                                  <i class="fa fa-building-o"></i>
                                  <span class="stats_info_text">Companies</span>
                                  <span class="stats_info_total">30</span>
                                </a>
                              </div>
                              <div class="col-md-6">
                                <a href="#" class="total_earnig">
                                  <i class="fa fa-money"></i>
                                  <span class="stats_info_text">Total Earings</span>
                                  <span class="stats_info_total">$ 200.11</span>
                                </a>
                            </div>
                            </div>
                            </div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="panel panel-default">
                           <div class="panel-heading"><i class="fa fa-area-chart"></i> Ride Statistics</div>
                            <div class="panel-body">
                            <div class="row">
                              <div class="col-md-6">
                                <a href="#" class="total_rides">
                                  <i class="fa fa-cubes"></i>
                                  <span class="stats_info_text">Total Rides</span>
                                  <span class="stats_info_total">130</span>
                                </a>
                              </div>
                              <div class="col-md-6">
                                <a href="#" class="total_drivers">
                                  <i class="fa fa-clone"></i>
                                  <span class="stats_info_text">On going rides</span>
                                  <span class="stats_info_total">10</span>
                                </a>
                            </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <a href="#" class="total_compnis">
                                  <i class="fa fa-times-circle-o"></i>
                                  <span class="stats_info_text">canceled rides</span>
                                  <span class="stats_info_total">20</span>
                                </a>
                              </div>
                              <div class="col-md-6">
                                <a href="#" class="total_earnig">
                                  <i class="fa fa-check"></i>
                                  <span class="stats_info_text">completed rides</span>
                                  <span class="stats_info_total">100</span>
                                </a>
                            </div>
                            </div>
                            </div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="panel panel-default">
                           <div class="panel-heading"><i class="fa fa-bar-chart"></i> Rides</div>
                             <div class="panel-body">
                            <div class="row">
                              <div class="col-md-6">
                                <a href="#" class="total_rides">
                                  <i class="fa fa-users"></i>
                                  <span class="stats_info_text">Riders</span>
                                  <span class="stats_info_total">100</span>
                                </a>
                              </div>
                              <div class="col-md-6">
                                <a href="#" class="total_drivers">
                                  <i class="fa fa-male"></i>
                                  <span class="stats_info_text">Drivers</span>
                                  <span class="stats_info_total">100</span>
                                </a>
                            </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <a href="#" class="total_compnis">
                                  <i class="fa fa-building-o"></i>
                                  <span class="stats_info_text">Companies</span>
                                  <span class="stats_info_total">30</span>
                                </a>
                              </div>
                              <div class="col-md-6">
                                <a href="#" class="total_earnig">
                                  <i class="fa fa-money"></i>
                                  <span class="stats_info_text">Total Earings</span>
                                  <span class="stats_info_total">$ 200.11</span>
                                </a>
                            </div>
                            </div>
                            </div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="panel panel-default">
                           <div class="panel-heading"><i class="fa fa-bar-chart"></i> Drivers</div>
                            <div class="panel-body">
                            <div class="row">
                              <div class="col-md-6">
                                <a href="#" class="total_rides">
                                  <i class="fa fa-users"></i>
                                  <span class="stats_info_text">Riders</span>
                                  <span class="stats_info_total">100</span>
                                </a>
                              </div>
                              <div class="col-md-6">
                                <a href="#" class="total_drivers">
                                  <i class="fa fa-male"></i>
                                  <span class="stats_info_text">Drivers</span>
                                  <span class="stats_info_total">100</span>
                                </a>
                            </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <a href="#" class="total_compnis">
                                  <i class="fa fa-building-o"></i>
                                  <span class="stats_info_text">Companies</span>
                                  <span class="stats_info_total">30</span>
                                </a>
                              </div>
                              <div class="col-md-6">
                                <a href="#" class="total_earnig">
                                  <i class="fa fa-money"></i>
                                  <span class="stats_info_text">Total Earings</span>
                                  <span class="stats_info_total">$ 200.11</span>
                                </a>
                            </div>
                            </div>
                            </div>
                        </div>
                      </div>
                      
                    </div>
                  </div><!-- /col-lg-12 END SECTION MIDDLE -->
              </div><!--/row -->
          </section>
      </section>

@endsection
